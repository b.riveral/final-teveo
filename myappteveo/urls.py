from django.contrib.auth.views import LoginView as login
from django.urls import path

from . import views


urlpatterns = [
    path('', views.home, name='home'),
    path('logout/', views.logout_view, name='logout'),
    path('media/comment_images/<str:image_id>', views.get_comments_images, name='get_comments_images'),
    path('cameras/', views.camera_list, name='camera_list'),
    path('data_sources/download/<str:source_name>/', views.process_data_source, name='process_data_source'),
    path('data_sources/', views.list_data_sources, name='list_data_sources'),
    path('cameras/<str:camera_id>-dyn/', views.camera_dynamic_page, name='camera_dynamic_page'),
    path('cameras/<str:camera_id>/', views.camera_detail, name='camera_detail'),
    path('cameras/<str:camera_id>/json/', views.camera_detail_json, name='camera_detail_json'),
    path('comentario/', views.add_comment, name='add_comment'),
    path('cameras/update_image/<str:camera_id>/', views.update_image, name='update_image'),
    path('cameras/update_comments/<str:camera_id>/', views.update_comments, name='update_comments'),
    path('cameras/<str:camera_id>/like/', views.like_camera, name='like_camera'),
    path('config/', views.site_config, name='site_config'),
    path('help/', views.help_page, name='help_page'),
    path('logout/', views.logout_view, name='logout_view'),
    path('login', login.as_view())
]