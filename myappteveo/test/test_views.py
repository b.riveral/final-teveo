
import os
import shutil
import uuid
from unittest.mock import patch

from django.conf import settings

from django.test import TestCase, Client, RequestFactory
from django.urls import reverse
from django.utils import timezone
from django.core.files.uploadedfile import SimpleUploadedFile

from ..models import Camera, Comment, Visitor, Like, SiteConfig
from ..views import get_or_create_visitor
from ..forms import SiteConfigForm


class LogoutViewTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.visitor = Visitor.objects.create(visitor_id=str(uuid.uuid4()), name_visitor='Test Visitor')

    def setUp(self):
        self.client = Client()
        session = self.client.session
        session['visitor_id'] = self.visitor.visitor_id
        session.save()

    def test_logout_status_code(self):
        response = self.client.get(reverse('logout_view'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))

    def test_logout_clears_session(self):
        response = self.client.get(reverse('logout_view'))
        self.assertNotIn('visitor_id', self.client.session)

    def test_logout_resets_visitor_name(self):
        # First, confirm the visitor name is 'Test Visitor'
        response = self.client.get(reverse('home'))
        self.assertContains(response, 'Test Visitor')

        self.client.get(reverse('logout_view'))

        # Confirm the visitor name is reset to 'Anónimo'
        response = self.client.get(reverse('home'))
        self.assertContains(response, 'Anónimo')


class GetOrCreateVisitorTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

    def test_new_visitor_session(self):
        request = self.factory.get('/')
        request.session = self.client.session

        visitor = get_or_create_visitor(request)

        self.assertIsNotNone(visitor)
        self.assertTrue(Visitor.objects.filter(visitor_id=visitor.visitor_id).exists())
        self.assertEqual(request.session['visitor_id'], visitor.visitor_id)

    def test_get_existing_visitor_session(self):
        # Create a visitor and set the session
        visitor_id = str(uuid.uuid4())
        visitor = Visitor.objects.create(visitor_id=visitor_id, name_visitor='Anonimo')

        session = self.client.session
        session['visitor_id'] = visitor_id
        session.save()

        request = self.factory.get('/')
        request.session = session

        retrieved_visitor = get_or_create_visitor(request)

        self.assertEqual(visitor, retrieved_visitor)
        self.assertEqual(request.session['visitor_id'], visitor_id)


class HomeViewTests(TestCase):
    def setUp(self):
        self.camera1 = Camera.objects.create(
            camera_id="CAM1",
            camera_info="Camera 1",
            location_lat=40.7128,
            location_lon=-74.0060,
            image_url="http://example.com/cam1.jpg"
        )
        self.camera2 = Camera.objects.create(
            camera_id="CAM2",
            camera_info="Camera 2",
            location_lat=34.0522,
            location_lon=-118.2437,
            image_url="http://example.com/cam2.jpg"
        )

        # Crear comentarios de prueba
        self.comment1 = Comment.objects.create(
            camera=self.camera1,
            comment="Comentario 1",
            comment_date=timezone.now(),
            image=SimpleUploadedFile("comment1.jpg", b"file_content", content_type="image/jpeg")
        )
        self.comment2 = Comment.objects.create(
            camera=self.camera2,
            comment="Comentario 2",
            comment_date=timezone.now() - timezone.timedelta(days=1),
            image=SimpleUploadedFile("comment2.jpg", b"file_content", content_type="image/jpeg")
        )

    def test_home_view_status_code(self):
        # Test for response in home page
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_home_view_template(self):
        # Test for template use in home page
        response = self.client.get(reverse('home'))
        self.assertTemplateUsed(response, 'home.html')

    def test_home_view_context(self):
        # Test for check context in home page
        response = self.client.get(reverse('home'))
        self.assertIn('page_obj', response.context)
        self.assertIn('cameras_count', response.context)
        self.assertIn('comments_count', response.context)

    def test_home_view_pagination(self):
        # Create some comments
        for i in range(10):
            Comment.objects.create(
                camera=self.camera1,
                comment=f"Comentario {i + 3}",
                comment_date=timezone.now() - timezone.timedelta(days=i + 2),
                image=SimpleUploadedFile(f"comment{i + 3}.jpg", b"file_content", content_type="image/jpeg")
            )

        response = self.client.get(reverse('home'))
        self.assertEqual(len(response.context['page_obj']), 10)

    def test_home_view_comments_order(self):
        response = self.client.get(reverse('home'))
        comments = response.context['page_obj'].object_list
        self.assertEqual(comments[0], self.comment1)
        self.assertEqual(comments[1], self.comment2)


class CameraListViewTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create data source directory if it does not exist
        cls.data_sources_dir = os.path.join(settings.BASE_DIR, 'data_sources')
        if not os.path.exists(cls.data_sources_dir):
            os.makedirs(cls.data_sources_dir)

        # Create test XML files
        cls.source1 = os.path.join(cls.data_sources_dir, 'listado1.xml')
        with open(cls.source1, 'w') as f:
            f.write('''<data>
                                <camara>
                                    <id>001</id>
                                    <lugar>Camera in the main square</lugar>
                                    <coordenadas>40.7128,-74.0060</coordenadas>
                                    <src>http://example.com/cam_001.jpg</src>
                                </camara>
                            </data>''')
        cls.source2 = os.path.join(cls.data_sources_dir, 'listado2.xml')
        with open(cls.source2, 'w') as f:
            f.write('''<data>
                                <camara>
                                    <id>002</id>
                                    <lugar>Camera at the park</lugar>
                                    <coordenadas>34.0522,-118.2437</coordenadas>
                                    <src>http://example.com/cam_002.jpg</src>
                                </camara>
                            </data>''')

        cls.camera1 = Camera.objects.create(
            camera_id='LIS1-001',
            camera_info='Camera in the main square',
            location_lat=40.7128,
            location_lon=-74.0060,
            image_url='http://example.com/cam_001.jpg',
            likes=10
        )

        cls.camera2 = Camera.objects.create(
            camera_id='LIS1-002',
            camera_info='Camera at the park',
            location_lat=34.0522,
            location_lon=-118.2437,
            image_url='http://example.com/cam_002.jpg',
            likes=5
        )

        # Include comments for each camera
        for i in range(15):
            Comment.objects.create(camera=cls.camera1, comment=f'Comment {i}', image='path/to/image')

        for i in range(5):
            Comment.objects.create(camera=cls.camera2, comment=f'Comment {i}', image='path/to/image')

    @classmethod
    def tearDownClass(cls):
        # Remove data source directory after testing
        shutil.rmtree(cls.data_sources_dir)
        super().tearDownClass()

    def setUp(self):
        self.client = Client()

    def test_camera_list_status_code(self):
        response = self.client.get(reverse('camera_list'))
        self.assertEqual(response.status_code, 200)

    def test_camera_list_template_used(self):
        response = self.client.get(reverse('camera_list'))
        self.assertTemplateUsed(response, 'camera_list.html')

    def test_camera_list_pagination(self):
        response = self.client.get(reverse('camera_list'))
        self.assertIn('page_obj', response.context)
        self.assertEqual(len(response.context['page_obj']), 2)

    def test_camera_list_order_by_comments(self):
        response = self.client.get(reverse('camera_list'))
        cameras = response.context['page_obj'].object_list
        self.assertEqual(cameras[0].camera_id, 'LIS1-001')
        self.assertEqual(cameras[1].camera_id, 'LIS1-002')

    def test_random_camera_in_context(self):
        response = self.client.get(reverse('camera_list'))
        self.assertIn('random_camera', response.context)
        random_camera = response.context['random_camera']
        self.assertIn(random_camera, Camera.objects.all())

    def test_data_sources_in_context(self):
        response = self.client.get(reverse('camera_list'))
        self.assertIn('data_sources', response.context)
        data_sources = response.context['data_sources']
        self.assertIn('listado1.xml', data_sources)
        self.assertIn('listado2.xml', data_sources)


class DataSourceTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Create data source directory if it does not exist
        cls.data_sources_dir = os.path.join(settings.BASE_DIR, 'test_data_sources')
        if not os.path.exists(cls.data_sources_dir):
            os.makedirs(cls.data_sources_dir)

        # Create test XML files
        cls.source1 = os.path.join(cls.data_sources_dir, 'listado1.xml')
        with open(cls.source1, 'w') as f:
            f.write('''<data>
                            <camara>
                                <id>1</id>
                                <lugar>Camera in the main square</lugar>
                                <coordenadas>40.7128,-74.0060</coordenadas>
                                <src>http://example.com/cam_001.jpg</src>
                            </camara>
                        </data>''')
        print(f"Archivo creado: {cls.source1}")

        cls.source2 = os.path.join(cls.data_sources_dir, 'listado2.xml')
        with open(cls.source2, 'w') as f:
            f.write('''<list>
                    <cam id="A">
                        <url>http://infocar.dgt.es/etraffic/data/camaras/3.jpg</url>
                        <info>CAMARA-CGT VALLADOLID_3</info>
                        <place>
                            <latitude>41.9811</latitude>
                            <longitude>-4.4348</longitude>
                        </place>
                    </cam>''')
        print(f"Archivo creado: {cls.source2}")

        cls.source3 = os.path.join(cls.data_sources_dir, 'camaras_euskadi.json')
        with open(cls.source3, 'w') as f:
            f.write('''[{
                      "INDEX" : "1",
                      "TITLE" : "Iurreta",
                      "URLCAM" : "https://www.trafikoa.eus/static/files/tr/camaras/819.jpg",
                      "LATWGS84" : "43.18725",
                      "LONWGS84" : "-2.673754",
                      "ROAD" : "A-8",
                      "PK" : "91.000"
                    }, {
                      "INDEX" : "2",
                      "TITLE" : "Montorra",
                      "URLCAM" : "https://www.trafikoa.eus/static/files/tr/camaras/820.jpg",
                      "LATWGS84" : "43.21167",
                      "LONWGS84" : "-2.719359",
                      "ROAD" : "A-8",
                      "PK" : "96.000"
                    }]''')
        print(f"Archivo creado: {cls.source3}")

    @classmethod
    def tearDownClass(cls):
        # Remove data source directory after testing
        shutil.rmtree(cls.data_sources_dir)
        super().tearDownClass()

    def setUp(self):
        self.client = Client()

    def test_list_data_sources(self):
        response = self.client.get(reverse('list_data_sources'))
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'listado1.xml')
        self.assertContains(response, 'listado2.xml')
        self.assertContains(response, 'camaras_euskadi.json')

    def test_process_xml_data_source(self):
        response = self.client.get(reverse('process_data_source', args=['listado1.xml']))
        self.assertEqual(response.status_code, 302)  # Redirection

        # Verify that the data has been stored in the database
        self.assertTrue(Camera.objects.filter(camera_id='LIS1-1').exists())
        cam_001 = Camera.objects.get(camera_id='LIS1-1')
        self.assertEqual(cam_001.camera_info, 'TV24-PTE. MEDITERRÁNEO')
        self.assertEqual(cam_001.location_lat, -4.424923000000033)
        self.assertEqual(cam_001.location_lon, 36.73751199999788)
        self.assertEqual(cam_001.image_url, 'https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg')
        self.assertEqual(cam_001.likes, 0)

        # Check that no data is duplicated when processing the same file again
        response = self.client.get(reverse('process_data_source', args=['listado1.xml']))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Camera.objects.filter(camera_id='LIS1-1').count(), 1)

    def test_process_json_data_source(self):
        response = self.client.get(reverse('process_data_source', args=['camaras_euskadi.json']))
        self.assertEqual(response.status_code, 302)  # Redirection

        # Verify that the data has been stored in the database
        self.assertTrue(Camera.objects.filter(camera_id='Euskadi-1').exists())
        cam_001 = Camera.objects.get(camera_id='Euskadi-1')
        self.assertEqual(cam_001.camera_info, 'Iurreta A-8 91.000')
        self.assertEqual(cam_001.location_lat, 43.18725)
        self.assertEqual(cam_001.location_lon, -2.673754)
        self.assertEqual(cam_001.image_url, 'https://www.trafikoa.eus/static/files/tr/camaras/819.jpg')
        self.assertEqual(cam_001.likes, 0)

        # Check that no data is duplicated when processing the same file again
        response = self.client.get(reverse('process_data_source', args=['camaras_euskadi.json']))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Camera.objects.filter(camera_id='Euskadi-1').count(), 1)


class CameraDetailViewsTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.camera = Camera.objects.create(
            camera_id='LIS1-050',
            camera_info='Camera in the main square',
            location_lat=40.7128,
            location_lon=-74.0060,
            image_url='http://example.com/cam_001.jpg',
            likes=10
        )

        # Test comments
        for i in range(5):
            Comment.objects.create(camera=cls.camera, comment=f'Comment {i}', image='path/to/image')

        # Test visitor
        cls.visitor = Visitor.objects.create(visitor_id='visitor_1', name_visitor='Test Visitor')

    def setUp(self):
        self.client = Client()
        session = self.client.session
        session['visitor_id'] = 'visitor_1'
        session.save()

    def test_camera_detail_status_code(self):
        response = self.client.get(reverse('camera_detail', args=['LIS1-050']))
        self.assertEqual(response.status_code, 200)

    def test_camera_detail_template_used(self):
        response = self.client.get(reverse('camera_detail', args=['LIS1-050']))
        self.assertTemplateUsed(response, 'camera_detail.html')

    def test_camera_detail_content(self):
        response = self.client.get(reverse('camera_detail', args=['LIS1-050']))
        self.assertContains(response, 'Camera in the main square')
        self.assertContains(response, '40.7128')
        self.assertContains(response, '-74.006')
        self.assertContains(response, 'http://example.com/cam_001.jpg')
        self.assertContains(response, 'Comment 0')
        self.assertContains(response, 'Comment 4')

    def test_like_camera(self):
        response = self.client.post(reverse('like_camera', args=['LIS1-050']))
        # Redirection
        self.assertEqual(response.status_code, 302)
        camera = Camera.objects.get(camera_id='LIS1-050')
        self.assertEqual(camera.likes, 11)
        like_exists = Like.objects.filter(visitor=self.visitor, camera=camera).exists()
        self.assertTrue(like_exists)


class AddCommentViewsTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.camera = Camera.objects.create(
            camera_id='LIS1-001',
            camera_info='Camera in the main square',
            location_lat=40.7128,
            location_lon=-74.0060,
            image_url='http://example.com/cam_001.jpg',
            likes=10
        )
        cls.visitor = Visitor.objects.create(visitor_id=str(uuid.uuid4()), name_visitor='Pepe')

    def setUp(self):
        self.client = Client()
        session = self.client.session
        session['visitor_id'] = self.visitor.visitor_id
        session.save()

    def test_add_comment_status_code(self):
        response = self.client.get(reverse('add_comment') + '?camera_id=LIS1-001')
        self.assertEqual(response.status_code, 200)

    def test_add_comment_template_used(self):
        response = self.client.get(reverse('add_comment') + '?camera_id=LIS1-001')
        self.assertTemplateUsed(response, 'add_comment.html')

    def test_add_comment_content(self):
        response = self.client.get(reverse('add_comment') + '?camera_id=LIS1-001')
        self.assertContains(response, 'Camera in the main square')
        self.assertContains(response, '40.7128')
        self.assertContains(response, '-74.006')
        self.assertContains(response, 'http://example.com/cam_001.jpg')

    @patch('requests.get')
    def test_add_comment_form_submission(self, mock_get):
        mock_get.return_value.status_code = 200
        mock_get.return_value.content = b'fake_image_content'

        response = self.client.post(reverse('add_comment') + '?camera_id=LIS1-001', {
            'comment': 'Test Comment',
        })
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Comment.objects.filter(camera=self.camera, comment='Test Comment').exists())

        comment = Comment.objects.get(camera=self.camera, comment='Test Comment')
        self.assertTrue(comment.image)
        self.assertEqual(comment.visitor, self.visitor)


class CameraDynamicPageViewsTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.camera = Camera.objects.create(
            camera_id='LIS1-001',
            camera_info='Camera in the main square',
            location_lat=40.7128,
            location_lon=-74.0060,
            image_url='http://example.com/cam_001.jpg',
            likes=10
        )

        for i in range(5):
            Comment.objects.create(camera=cls.camera, comment=f'Comment {i}', image='path/to/image')

    def setUp(self):
        self.client = Client()

    def test_camera_dynamic_page_status_code(self):
        response = self.client.get(reverse('camera_dynamic_page', args=['LIS1-001']))
        self.assertEqual(response.status_code, 200)

    def test_camera_dynamic_page_content(self):
        response = self.client.get(reverse('camera_dynamic_page', args=['LIS1-001']))
        self.assertContains(response, 'Camera in the main square')
        self.assertContains(response, '40.7128')
        self.assertContains(response, '-74.006')
        self.assertContains(response, 'http://example.com/cam_001.jpg')
        self.assertContains(response, 'Comment 0')
        self.assertContains(response, 'Comment 4')

    @patch('requests.get')
    def test_add_comment_via_htmx(self, mock_get):
        mock_get.return_value.status_code = 200
        mock_get.return_value.content = b'fake_image_content'

        response = self.client.post(reverse('add_comment') + '?camera_id=LIS1-001', {
            'comment': 'HTMX Test Comment',
        }, **{'HTTP_HX-Request': 'true'})

        self.assertEqual(response.status_code, 204)
        self.assertTrue(Comment.objects.filter(camera=self.camera, comment='HTMX Test Comment').exists())
        comment = Comment.objects.get(camera=self.camera, comment='HTMX Test Comment')
        self.assertTrue(comment.image)

    def test_update_image(self):
        response = self.client.get(reverse('update_image', args=['LIS1-001']))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'http://example.com/cam_001.jpg')

    def test_update_comments(self):
        response = self.client.get(reverse('update_comments', args=['LIS1-001']))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Comment 0')
        self.assertContains(response, 'Comment 4')


class SiteConfigViewTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.visitor = Visitor.objects.create(visitor_id=str(uuid.uuid4()), name_visitor='Anonimo')
        cls.config = SiteConfig.objects.create(
            visitor=cls.visitor,
            comment_name='Usuario',
            font_size='16px',
            font_family='Arial'
        )

    def setUp(self):
        self.client = Client()
        session = self.client.session
        session['visitor_id'] = self.visitor.visitor_id
        session.save()

    def test_site_config_load(self):
        response = self.client.get(reverse('site_config'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Configuración del Sitio')
        self.assertContains(response, 'Usuario')
        self.assertContains(response, '16px')
        self.assertContains(response, 'Arial')

    def test_site_config_save(self):
        data = {
            'comment_name': 'NuevoNombre',
            'font_size': '18px',
            'font_family': 'Helvetica'
        }
        response = self.client.post(reverse('site_config'), data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))

        config = SiteConfig.objects.get(visitor=self.visitor)
        self.assertEqual(config.comment_name, 'NuevoNombre')
        self.assertEqual(config.font_size, '18px')
        self.assertEqual(config.font_family, 'Helvetica')

    def test_site_config_form_render(self):
        response = self.client.get(reverse('site_config'))
        self.assertIsInstance(response.context['form'], SiteConfigForm)
        self.assertContains(response, 'name="comment_name"')
        self.assertContains(response, 'name="font_size"')
        self.assertContains(response, 'name="font_family"')
