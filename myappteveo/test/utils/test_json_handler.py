import unittest
import os

from django.conf import settings

from ...utils.json_handler import JsonHandlerEuskadi, JsonHandlerDonostia, JsonHandlerVitoria, JsonHandlerZaragoza


test_data_sources_dir = os.path.join(settings.BASE_DIR, 'myappteveo/test/test_files/')


class TestJsonHandlers(unittest.TestCase):

    def test_json_handler_euskadi(self):
        test_file = 'test_euskadi.json'

        test_euskadi = JsonHandlerEuskadi(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_json()

        expected = [
            {
                'camera_id': 'Euskadi-1',
                'camera_info': 'Iurreta A-8 91.000',
                'location_lat': '43.18725',
                'location_lon': '-2.673754',
                'image_url': 'https://www.trafikoa.eus/static/files/tr/camaras/819.jpg',
            },
            {
                'camera_id': 'Euskadi-2',
                'camera_info': 'Montorra A-8 96.000',
                'location_lat': '43.21167',
                'location_lon': '-2.719359',
                'image_url': 'https://www.trafikoa.eus/static/files/tr/camaras/820.jpg',
            }
        ]

        self.assertEqual(test_euskadi, expected)

    def test_json_handler_donostia(self):

        test_file = 'test_donostia.json'

        test_donostia = JsonHandlerDonostia(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_json()

        expected = [
            {
                'camera_id': 'Donostia-Amara',
                'camera_info': 'GI-20 11.000',
                'location_lat': '43.29769',
                'location_lon': '-1.986157',
                'image_url': 'http://www.trafikoa.net/static/files/tr/camaras/807.jpg',
            },
            {
                'camera_id': 'Donostia-A�orga',
                'camera_info': 'GI-20 12.000',
                'location_lat': '43.295609',
                'location_lon': '-1.997044',
                'image_url': 'http://www.trafikoa.net/static/files/tr/camaras/808.jpg',
            }
        ]

        self.assertEqual(test_donostia, expected)

    def test_json_handler_vitoria(self):
        test_file = 'test_vitoria.json'

        test_vitoria = JsonHandlerVitoria(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_json()

        expected = [
            {
                'camera_id': 'Vitoria-CAM30',
                'camera_info': 'Antonia Fija 1 - Avenida Zabalgana',
                'location_lat': '-2.696882',
                'location_lon': '42.838785',
                'image_url': 'https://www.vitoria-gasteiz.org/c11-01w/cameras?action=get&id=CAM30',
            },
            {
                'camera_id': 'Vitoria-CAM31',
                'camera_info': 'Antonia Fija 2 - Alto Armentia',
                'location_lat': '-2.697583',
                'location_lon': '42.837707',
                'image_url': 'https://www.vitoria-gasteiz.org/c11-01w/cameras?action=get&id=CAM31',
            }
        ]

        self.assertEqual(test_vitoria, expected)

    def test_json_handler_zaragoza(self):
        test_file = 'test_zaragoza.json'

        test_zaragoza = JsonHandlerZaragoza(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_json()

        expected = [
            {
                'camera_id': 'Zaragoza-799',
                'camera_info': 'C/ Porvenir, lado impares CALLE PORVENIR',
                'location_lat': '676059.061046388',
                'location_lon': '4611936.47996247',
                'image_url': 'https://www.zaragoza.es/sede/servicio/via-publica/incidencia/799',
            }
        ]

        self.assertEqual(test_zaragoza, expected)


