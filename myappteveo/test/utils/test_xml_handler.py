import unittest
import os

from django.conf import settings

from ...utils.xml_handler import XmlHandlerList1, XmlHandlerList2, XmlHandlerDGT, XmlHandlerMC30


test_data_sources_dir = os.path.join(settings.BASE_DIR, 'myappteveo/test/test_files/')


class TestXmlHandlers(unittest.TestCase):

    def test_xml_handler_list1(self):
        test_file = 'test_listado1.xml'

        listado1 = XmlHandlerList1(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_xml()

        expected = [
            {
                'camera_id': 'LIS1-1',
                'camera_info': 'Location 1',
                'location_lat': '10.0',
                'location_lon': '20.0',
                'image_url': 'http://example.com/image1.jpg',
            },
            {
                'camera_id': 'LIS1-2',
                'camera_info': 'Location 2',
                'location_lat': '30.0',
                'location_lon': '40.0',
                'image_url': 'http://example.com/image2.jpg',
            }
        ]

        self.assertEqual(listado1, expected)

    def test_xml_handler_list2(self):

        test_file = 'test_listado2.xml'

        listado2 = XmlHandlerList2(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_xml()

        expected = [
            {
                'camera_id': 'LIS2-A',
                'camera_info': 'Info A',
                'location_lat': '50.0',
                'location_lon': '60.0',
                'image_url': 'http://example.com/imageA.jpg',
            },
            {
                'camera_id': 'LIS2-B',
                'camera_info': 'Info B',
                'location_lat': '70.0',
                'location_lon': '80.0',
                'image_url': 'http://example.com/imageB.jpg',
            }
        ]

        self.assertEqual(listado2, expected)

    def test_xml_handler_dgt(self):
        test_file = 'test_dgt.xml'

        dgt = XmlHandlerDGT(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_xml()

        expected = [
            {
                'camera_id': '2',
                'camera_info': 'CAMARA-CGT VALLADOLID_2',
                'location_lat': '42.0676',
                'location_lon': '-4.2227',
                'image_url': 'http://infocar.dgt.es/etraffic/data/camaras/2.jpg',
            }
        ]

        self.assertEqual(dgt, expected)

    def test_xml_handler_mc30(self):
        test_file = 'test_mc30.xml'

        mc30 = XmlHandlerMC30(base_dir=test_data_sources_dir, file_name=test_file).get_element_from_xml()

        expected = [
            {
                'camera_id': 'MC30-09NC39TV01',
                'camera_info': '09NC39TV01',
                'location_lat': '40.40376974',
                'location_lon': '-3.66657048',
                'image_url': 'www.mc30.es/components/com_hotspots/datos/imagenes_camaras/09NC39TV01.jpg',
            },
            {
                'camera_id': 'MC30-15RR64TV01',
                'camera_info': '15RR64TV01',
                'location_lat': '40.40016472',
                'location_lon': '-3.71215353',
                'image_url': 'www.mc30.es/components/com_hotspots/datos/imagenes_camaras/15RR64TV01.jpg',
            }
        ]

        self.assertEqual(mc30, expected)


