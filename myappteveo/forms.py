from django import forms
from .models import Comment, SiteConfig, Visitor


class CommentForm(forms.ModelForm):
    image = forms.ImageField(required=False)

    class Meta:
        model = Comment
        fields = ['comment', 'image']


class SiteConfigForm(forms.ModelForm):
    class Meta:
        model = SiteConfig
        fields = ['comment_name', 'font_size', 'font_family']
        widgets = {
            'comment_name': forms.TextInput(attrs={'class': 'form-control'}),
            'font_size': forms.TextInput(attrs={'class': 'form-control'}),
            'font_family': forms.TextInput(attrs={'class': 'form-control'}),
        }
