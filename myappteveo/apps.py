from django.apps import AppConfig


class MyappteveoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'myappteveo'
