import abc
import json
from abc import ABC
import os


class JsonHandler(ABC):

    def __init__(self, base_dir, file_name):
        self.file_name = file_name
        self.base_dir = base_dir

        self.file_path = os.path.join(self.base_dir, self.file_name)

    @staticmethod
    def _get_coordinates(text):
        return text.split(",")

    def parser_json(self):
        with open(self.file_path, 'r', encoding='utf-8') as file:
            data = json.load(file)
        return data

    @abc.abstractmethod
    def get_element_from_json(self):
        raise NotImplementedError("Subclasses must implement this method")


class JsonHandlerEuskadi(JsonHandler):
    """
        Class handling the JSON file os the data source Euskadi.
    """
    PREFIX = "Euskadi"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    def get_element_from_json(self):
        root = self.parser_json()

        cameras = []
        for cam in root:
            camera_info = {
                'camera_id': self.PREFIX + "-" + cam.get('INDEX'),
                'camera_info': f'{cam.get("TITLE")} {cam.get("ROAD")} {cam.get("PK")}',
                'location_lat': cam.get('LATWGS84'),
                'location_lon': cam.get('LONWGS84'),
                'image_url': cam.get('URLCAM'),
            }
            cameras.append(camera_info)

        return cameras


class JsonHandlerDonostia(JsonHandler):
    """
        Class handling the JSON file os the data source Donostia.
    """
    PREFIX = "Donostia"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    def get_element_from_json(self):
        root = self.parser_json()

        cameras = []
        for cam in root:
            camera_info = {
                'camera_id': self.PREFIX + "-" + cam.get('Nombre'),
                'camera_info': f'{cam.get("Carretera")} {cam.get("PK")}',
                'location_lat': cam.get('Latitud'),
                'location_lon': cam.get('Longitud'),
                'image_url': cam.get('Imagen'),
            }
            cameras.append(camera_info)

        return cameras


class JsonHandlerVitoria(JsonHandler):
    """
        Class handling the JSON file os the data source Vitoria.
    """
    PREFIX = "Vitoria"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    def get_element_from_json(self):
        root = self.parser_json()

        cameras = []
        for feature in root.get('features', []):

            camera_info = {
                'camera_id': self.PREFIX + "-" + feature.get('id'),
                'camera_info': feature["properties"].get("nombre"),
                'location_lat': f'{feature["geometry"].get("coordinates")[0]}',
                'location_lon': f'{feature["geometry"].get("coordinates")[1]}',
                'image_url': feature["properties"].get("imagen"),
            }
            cameras.append(camera_info)

        return cameras


class JsonHandlerZaragoza(JsonHandler):
    """
        Class handling the JSON file os the data source Vigo.
    """
    PREFIX = "Zaragoza"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    def get_element_from_json(self):
        root = self.parser_json()

        camera_info = [{
            'camera_id': f'{self.PREFIX}-{root.get("id")}',
            'camera_info': f'{root.get("title")} {root.get("calle")}',
            'location_lat': f'{root["geometry"].get("coordinates")[0]}',
            'location_lon': f'{root["geometry"].get("coordinates")[1]}',
            'image_url': root.get('uri'),
        }]

        return camera_info
