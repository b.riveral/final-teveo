import abc
from abc import ABC
from lxml import etree
import os


class XmlHandler(ABC):

    def __init__(self, base_dir, file_name):
        self.file_name = file_name
        self.base_dir = base_dir

        self.file_path = os.path.join(self.base_dir, self.file_name)

    def parser_xml(self):
        # Read in binary mode
        with open(self.file_path, 'rb') as file:
            content = file.read()

        # XML parser
        root = etree.fromstring(content)
        return root

    @abc.abstractmethod
    def get_element_from_xml(self):
        raise NotImplementedError("Subclasses must implement this method")


class XmlHandlerList1(XmlHandler):
    """
        Class handling the XML file os the data source Listado 1.
    """
    PREFIX = "LIS1"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    @staticmethod
    def _get_coordinates(text):
        return text.split(",")

    def get_element_from_xml(self):
        root = self.parser_xml()

        cameras = []
        for camera in root.findall('camara'):
            # Find coordinates
            coordinates = self._get_coordinates(camera.find('coordenadas').text)

            camera_info = {
                'camera_id': self.PREFIX + "-" + camera.find('id').text,
                'camera_info': camera.find('lugar').text,
                'location_lat': coordinates[0],
                'location_lon': coordinates[1],
                'image_url': camera.find('src').text,
            }
            cameras.append(camera_info)

        return cameras


class XmlHandlerList2(XmlHandler):
    """
        Class handling the XML file os the data source Listado 2.
    """
    PREFIX = "LIS2"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    def get_element_from_xml(self):
        root = self.parser_xml()

        cameras = []
        for cam in root.findall('cam'):
            camera_info = {
                'camera_id': self.PREFIX + "-" + cam.get('id'),
                'camera_info': cam.find('info').text,
                'location_lat': cam.find('place/latitude').text,
                'location_lon': cam.find('place/longitude').text,
                'image_url': cam.find('url').text,
            }
            cameras.append(camera_info)

        return cameras


class XmlHandlerDGT(XmlHandler):
    """
        Class handling the XML file os the data source DGT.
    """
    PREFIX = "DGT"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    def get_element_from_xml(self):
        root = self.parser_xml()

        cameras = []
        # Namespace handler required for _0
        namespace = {'_0': 'http://datex2.eu/schema/2/2_0'}
        for camera in root.findall('.//_0:cctvCameraMetadataRecord', namespaces=namespace):
            camera_info = {
                'camera_id': camera.find('_0:cctvCameraSerialNumber', namespaces=namespace).text,
                'camera_info': camera.find('_0:cctvCameraIdentification', namespaces=namespace).text,
                'location_lat': camera.find('.//_0:latitude', namespaces=namespace).text,
                'location_lon': camera.find('.//_0:longitude', namespaces=namespace).text,
                'image_url': camera.find('.//_0:urlLinkAddress', namespaces=namespace).text,
            }
            cameras.append(camera_info)

        return cameras


class XmlHandlerMC30(XmlHandler):
    """
        Class handling the XML file os the data source Ayuntamiento de Madrid.
    """
    PREFIX = "MC30"

    def __init__(self, base_dir, file_name):
        super().__init__(base_dir=base_dir, file_name=file_name)

    def get_element_from_xml(self):
        root = self.parser_xml()

        cameras = []
        for cam in root.findall('Camara'):
            camera_info = {
                'camera_id': self.PREFIX + "-" + cam.find('Nombre').text,
                'camera_info': cam.find('Nombre').text,
                'location_lat': cam.find('Posicion/Latitud').text,
                'location_lon': cam.find('Posicion/Longitud').text,
                'image_url': cam.find('URL').text,
            }
            cameras.append(camera_info)

        return cameras