from ..models import SiteConfig, Visitor, Camera, Comment


def site_config(request):
    visitor_id = request.session.get('visitor_id')
    visitor = Visitor.objects.filter(visitor_id=visitor_id).first()
    config = SiteConfig.objects.filter(visitor=visitor).first()

    if not visitor:
        visitor = Visitor.objects.create(visitor_id='default', name_visitor='Anónimo')
        config = SiteConfig.objects.create(visitor=visitor)

    cameras_count = Camera.objects.count()
    comments_count = Comment.objects.count()

    return {
        'visitor_name': visitor.name_visitor,
        'site_config': config,
        'cameras_count': cameras_count,
        'comments_count': comments_count,
    }