from django.contrib import admin

from .models import Camera, Comment, Visitor, DataSource, Like, SiteConfig


class CameraAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['camera_id', 'camera_info']}),
        ('Camera information', {'fields': ['location_lat', 'location_lon', 'image_url', 'likes'],
                                'classes': ['collapse']}),
    ]


admin.site.register(Camera, CameraAdmin)
admin.site.register(Comment)
admin.site.register(Visitor)
admin.site.register(DataSource)
admin.site.register(Like)
admin.site.register(SiteConfig)
