
import uuid
import os
import random
import requests

from django.conf import settings
from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.core.files.base import ContentFile
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Count
from django.utils import timezone
from django.contrib import messages

from .models import Comment, Camera, Visitor, Like, SiteConfig
from .forms import CommentForm, SiteConfigForm
from .utils.json_handler import JsonHandlerEuskadi, JsonHandlerDonostia, JsonHandlerVitoria, JsonHandlerZaragoza
from .utils.xml_handler import XmlHandlerList1, XmlHandlerList2, XmlHandlerDGT, XmlHandlerMC30


DATA_SOURCE_CLASSES = {
    'listado1.xml': XmlHandlerList1,
    'listado2.xml': XmlHandlerList2,
    'MC30.xml': XmlHandlerMC30,
    'DGT.xml': XmlHandlerDGT,
    'camaras_euskadi.json': JsonHandlerEuskadi,
    'camaras_donostia.json': JsonHandlerDonostia,
    'camaras_vitoria.json': JsonHandlerVitoria,
    'camaras_zaragoza.json': JsonHandlerZaragoza
}


def get_or_create_visitor(request):
    """
        Saves the visitor's session to the page
    """
    # Check if the visitor already has a visitor_id in the session
    visitor_id = request.session.get('visitor_id')

    if not visitor_id:
        visitor = Visitor.objects.create(visitor_id=str(uuid.uuid4()))
        request.session['visitor_id'] = visitor.visitor_id
        visitor.save()
    else:
        visitor, _ = Visitor.objects.get_or_create(visitor_id=visitor_id)
    return visitor


def get_comments_images(request, image_id):
    image_sources_dir = os.path.join(settings.MEDIA_ROOT, 'comment_images')
    image = os.path.join(image_sources_dir, image_id)

    with open(image, "rb") as f:
        image_data = f.read()
    return HttpResponse(image_data, content_type='image/jpg')


def logout_view(request):
    """
        Logout
    """
    # Clear the session
    request.session.flush()
    logout(request)
    return redirect('home')


def home(request):
    """
        Views for home page
    """
    # Organize comments
    all_comments = Comment.objects.order_by('-comment_date')

    # Paginate comments. 10 comments for page
    paginator = Paginator(all_comments, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {'page_obj': page_obj,
               'cameras_count': Camera.objects.count(),
               'comments_count': Comment.objects.count()}

    return render(request, 'home.html', context)


def list_data_sources(request):
    """
        Displays the resources available for download on the camera page
    """
    data_sources_dir = os.path.join(settings.BASE_DIR, 'data_sources')
    data_sources = [f for f in os.listdir(data_sources_dir) if f.endswith(('.xml', '.json'))]
    return render(request, 'data_sources.html', {'data_sources': data_sources})


def process_data_source(request, source_name):
    """
        Download information on available resources
    """
    data_sources_dir = os.path.join(settings.BASE_DIR, 'data_sources')

    # Search for the corresponding data source in DATA_SOURCE_CLASSES
    data_source_class = DATA_SOURCE_CLASSES.get(source_name)
    if not data_source_class:
        return HttpResponse("Data source not found", status=404)

    data_source_instance = data_source_class(data_sources_dir, source_name)
    if data_source_instance.__class__.__name__.startswith('Xml'):
        elements = data_source_instance.get_element_from_xml()
    else:
        print("Hay elementos json")
        elements = data_source_instance.get_element_from_json()

    for element in elements:
        camera_id = element['camera_id']

        # If the camera does not exist, the database is updated
        if not Camera.objects.filter(camera_id=camera_id).exists():
            Camera.objects.create(
                camera_id=camera_id,
                camera_info=element['camera_info'],
                location_lat=element['location_lat'],
                location_lon=element['location_lon'],
                image_url=element['image_url']
            )
            print(f"Camera {camera_id} created")

    return redirect('list_data_sources')


def camera_list(request):
    """
        Views for cameras page
    """
    # Paginate cameras
    all_cameras = Camera.objects.annotate(comment_count=Count('comment')).order_by('-comment_count')
    # Show 10 cameras for page
    paginator = Paginator(all_cameras, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    # Random camera shown on top
    random_camera = random.choice(all_cameras) if all_cameras else None

    context = {
        'random_camera': random_camera,
        'page_obj': page_obj,
        'data_sources': [f for f in os.listdir(os.path.join(settings.BASE_DIR, 'data_sources')) if f.endswith(('.xml', '.json'))]
    }
    return render(request, 'camera_list.html', context)


def camera_detail(request, camera_id):
    """
        Views for camera detail
    """
    camera = get_object_or_404(Camera, camera_id=camera_id)
    comments = camera.comment_set.all().order_by('-comment_date')

    visitor = get_or_create_visitor(request)

    liked = Like.objects.filter(visitor=visitor, camera=camera).exists()
    return render(request, 'camera_detail.html',
                  {'camera': camera, 'comments': comments, 'liked': liked})


def camera_detail_json(request, camera_id):
    camera = get_object_or_404(Camera, camera_id=camera_id)
    comments_count = Comment.objects.filter(camera=camera).count()

    camera_data = {
        'camera_id': camera.camera_id,
        'camera_info': camera.camera_info,
        'location_lat': camera.location_lat,
        'location_lon': camera.location_lon,
        'image_url': camera.image_url,
        'likes': camera.likes,
        'comments_count': comments_count,
    }

    return JsonResponse(camera_data)


@login_required
def add_comment(request):
    """
        Views for added coments pages.
    """
    camera_id = request.GET.get('camera_id')
    camera = get_object_or_404(Camera, camera_id=camera_id)

    visitor = Visitor.objects.filter(user=request.user).first()

    if request.method == 'POST':
        form = CommentForm(request.POST, request.FILES)
        if form.is_valid():
            # Don't save the instance of the comment in the database in case something needs to be modified
            comment = form.save(commit=False)
            comment.camera = camera
            comment.visitor = visitor
            comment.comment_date = timezone.now()

            # Update the camera image
            if not comment.image:
                response = requests.get(camera.image_url)
                if response.status_code == 200:
                    image_name = os.path.basename(camera.image_url)
                    comment.image.save(image_name, ContentFile(response.content), save=True)

            comment.save()
            if 'Hx-Request' in request.headers:
                return HttpResponse(status=204)
            return redirect('camera_dynamic_page', camera_id=camera_id)
    else:
        form = CommentForm()

    context = {'camera': camera, 'form': form, 'current_time': timezone.now()}

    if 'Hx-Request' in request.headers:
        return render(request, 'comment_form.html', context)
    else:
        return render(request, 'add_comment.html', context)


@login_required
def like_camera(request, camera_id):
    if request.method == 'POST':
        camera = get_object_or_404(Camera, camera_id=camera_id)
        visitor = get_or_create_visitor(request)

        if not Like.objects.filter(visitor=visitor, camera=camera).exists():
            Like.objects.create(visitor=visitor, camera=camera)
            camera.likes += 1
            camera.save()
        return redirect('camera_detail', camera_id=camera_id)
    return HttpResponseBadRequest("Invalid request method.")


def camera_dynamic_page(request, camera_id):
    camera = get_object_or_404(Camera, camera_id=camera_id)
    comments = camera.comment_set.all().order_by('-comment_date')
    visitor = get_or_create_visitor(request)

    liked = Like.objects.filter(visitor=visitor, camera=camera).exists()

    return render(request, 'camera_dynamic_page.html',
                  {'camera': camera, 'comments': comments, 'liked': liked})


def update_image(request, camera_id):
    camera = get_object_or_404(Camera, camera_id=camera_id)
    return render(request, 'dynamic/image.html', {'camera': camera})


def update_comments(request, camera_id):
    camera = get_object_or_404(Camera, camera_id=camera_id)
    comments = camera.comment_set.all().order_by('-comment_date')
    return render(request, 'dynamic/comments.html', {'comments': comments})


def site_config(request):
    """
        Views for config page
    """
    visitor = get_or_create_visitor(request)
    config, created = SiteConfig.objects.get_or_create(visitor=visitor)
    if request.method == 'POST':
        form = SiteConfigForm(request.POST, instance=config)
        if form.is_valid():
            form.save()
            visitor.name_visitor = form.cleaned_data['comment_name']
            visitor.save()
            messages.success(request, 'Configuración guardada correctamente.')
            return redirect('home')
    else:
        form = SiteConfigForm(instance=config)

    context = {
        'form': form,
        'config': config,
    }
    return render(request, 'site_config.html', context)


def help_page(request):
    """
        Views for help page
    """
    return render(request, 'help.html')


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()

            visitor, created = Visitor.objects.get_or_create(user=user)
            visitor.name_visitor = user.username
            visitor.save()

            login(request, user)
            messages.success(request, 'Usuario registrado correctamente!')
            return redirect('home')
    else:
        form = UserCreationForm()

    return render(request, 'registration/register.html', {'form': form})

