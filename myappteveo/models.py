from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Visitor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    visitor_id = models.CharField(max_length=50)
    name_visitor = models.CharField(max_length=100)

    def __str__(self):
        return self.name_visitor


class Camera(models.Model):
    camera_id = models.CharField(max_length=100, null=False)
    camera_info = models.TextField(default='')
    location_lat = models.FloatField(default=0.0)
    location_lon = models.FloatField(default=0.0)
    image_url = models.URLField()
    likes = models.IntegerField(default=0)

    def __str__(self):
        return self.camera_id

    def comment_count(self):
        return self.comment_set.count()

    def like_count(self):
        return self.like_set.count()


class Comment(models.Model):
    camera = models.ForeignKey(Camera, on_delete=models.CASCADE)
    visitor = models.ForeignKey(Visitor, on_delete=models.CASCADE)
    comment = models.TextField(default='')
    comment_date = models.DateTimeField(default=timezone.now)
    image = models.ImageField(upload_to='comment_images/')

    def __str__(self):
        return f"Comment for {self.camera.camera_id} by {self.visitor.name_visitor} at {self.comment_date}"


class DataSource(models.Model):
    url_data = models.URLField()


class Like(models.Model):
    visitor = models.ForeignKey('Visitor', on_delete=models.CASCADE)
    camera = models.ForeignKey('Camera', on_delete=models.CASCADE)

    class Meta:
        # Unique restrictions in combination of the two fields
        unique_together = ('visitor', 'camera')


class SiteConfig(models.Model):
    visitor = models.ForeignKey(Visitor, on_delete=models.CASCADE)
    comment_name = models.CharField(max_length=100, default='Usuario')
    font_size = models.CharField(max_length=10, default='16px')
    font_family = models.CharField(max_length=100, default='Arial')

    def __str__(self):
        return "Configuración del Sitio"




