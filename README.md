# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Beatriz Rivera López
* Titulación: Doble grado en Ingeniería de Sistemas de Telecomunicaciones y Administración y Dirección de Empresas
* Cuenta en laboratorios: briveral
* Cuenta URJC: b.riveral@alumnos.urjc.es
* Video básico (url): https://youtu.be/1lwM02Fu5SQ
* Video parte opcional (url): https://youtu.be/eWi5t8pXAt0
* Despliegue (url): briveral.pythonanywhere.com
* Contraseñas:
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
La página principa de la aplicación cuyo nombre es "Principal" se encuenta en el recurso "/". En esta página puede verse 
todos los comentarios que hay de las cámaras disponibles ordenados de más actual a menos. Estos comentarios se muestran 
junto a la fecha del comentario, el identificador de su cámara como enlace a la página de la cámara y la imagen de la cámara
en el momento en el que se puso el comentario.
Si pinchamos sobre Cámaras, cuyo recurso es "/cameras", veremos un listado de cámaras y fuentes disponibles. En primer lugar
aparece un listado de fuentes de datos disponibles que se podrán descargar si pinchamos sobre el botón Descargar. Después, aparecerá
un listado de las cámaras disponibles en la base de datos, con un enlace a la página de la cámara y a la página dinámica de la cámara
y con todos los comentarios sobre ella.
Si pinchamos sobre cualquier recurso de la cámara nos llevará al detalle de la cámara, viendo la imagen de la cámara en el momento
en el que entramos a la página de cámara, siendo posible añadir un comentario tanto en la página de la cámara como en su página dinámica y 
pudiendo obtener la información de la cámara en formato json. La diferencia entre ambas es que en la página dinámica de la cámara nos refresca la imagen 
de la cámara cada 30 segundos, mientras que la imagen que se carga en la página del detalle de la cámara es la imagen del
momento que obtenemos el recurso. Estos recursos se encuentran alojados en "cameras/camera_id/" y "cameras/camera_id_dyn".
En la página para añadir comentario, desde la que accedemos bien desde la página de cámara o bien desde la página dinámica de la cámara, 
podemos añadir un comentario a la cámara, redirigiéndonos a la página de cámara después.
En el enlace a la Configuración, cuyo recurso es "config/", podemos agregar un nombre de usuario para que aparezcan en los comentarios y cambiar 
el estilo de la fuente.
En la página de ayuda, cuyo recurso es "help/" encontramos una breve descripción del funcionamiento de la práctica y de su autora.
Desde el botón "Cerrar sesión" podemos cerrar la sesión si hemos introducido un nombre anteriormente desde la página de Configuración.
Y por último, desde la página Admin podemos administrar todas las tablas disponibles en la base de datos.

## Lista partes opcionales

* Test unitarios
* Paginación de la página de cámaras y de la página principal
* Inclusión de un favicon al sitio
* Cierre de sesiones
* Se permite votar las cámaras
* En las fuentes de datos, se tiene como recurso el xml de las cámaras de Madrid Centro 30.
